import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-photo-player',
  templateUrl: './photo-player.component.html',
  styleUrls: ['./photo-player.component.css']
})
export class PhotoPlayerComponent implements OnInit {

  photo: any;
  libelle: any;

  constructor(
    private dialogRef: MatDialogRef<PhotoPlayerComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.photo = data.photo;
      this.libelle = data.libelle;
    }

  ngOnInit() {
  }

}
